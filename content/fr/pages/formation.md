title: Stages
<<: !!inc/file ../dates.yml
eventFilterSlug: initiation-permaculture
-------------------
# Stages d'initiation à la permaculture

<img src="img/formation.jpg" class="portrait"/>

Un stage d'initiation permet en 2 jours d'apréhander l'art de vivre qu'est la permaculture au-delà des simples techniques et méthodes. C'est un bon moyen de découvrir les bases de l'écologie comment elle peut s'appliquer au jardin ainsi qu'à d'autres domaines, et le tout avec une approche ludique, interactive et collaborative!
J'adapte le stage en fonction des connaissances et des questions des participants tout en garantissant une immersion permaculturelle.

Tous les stages d'initiation sont à prix (presque) conscient et nécessitent pour l'inscription 50€ d’acompte et le remplissage du formulaire.
L'hébergement est à régler directement avec l'organisme d'accueil.

Chaque stage est unique en fonction des hôtes qui nous accueillent qui ont chacun leur apports et leur touche personnelle à apporter au stage.

[Les prochaines dates sont consultables !](agenda.html "button")


# Stages A la demande
Je peux aussi proposer un stage à la demande selon vos intérêt et à domicile dans votre structure. Si je considère que je n'ai pas les compétences adéquates pour répondre à vos attentes, j'ai certainement des contacts dans mon réseau avec lesquels je pourrai vous mettre en relation.

# Dates à venir

<%
const now = (new Date()).getTime();
const futureEvents = events.filter( (e) => e.dateEnd.getTime()>now && e.type.titleSlug === eventFilterSlug );
futureEvents.sort( (a,b) => a.dateStart.getTime() - b.dateStart.getTime() );
futureEvents.forEach(event=>{
_%>
<%- include('../fragments/event.ejs',{myEvent:event}); %>
<% }); _%>
