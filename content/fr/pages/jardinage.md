title: Jardinage
<<: !!inc/file ../temoignages.yml
-------------------
# Jardinage

<img src="img/jardinage1.jpg" class="portrait"/>


Nous pouvons vous faire découvrir dans votre jardin potager plusieurs applications des principes de la permaculture et du jardinage naturel, tels que :

    • sol toujours couvert de végétaux vivants ou en compostage de surface
    • amendement végétal comme en forêt et avec du compost
    • respect et encouragement de la vie du sol (arthropodes, bactéries, champignons, nématodes,….)
    • recherche d’économie d’eau
    • accueil du sauvage qui augmente la résilience des plantes potagères, et le potentiel d’interactions     
      bénéfiques entre elles
    • prendre le temps d’observer, de ressentir le jardin et son environnement
    • valoriser la diversité
    • embellir tout en favorisant la production de légumes
    • densification et association des cultures
    • récolter ses graines qui ont engrammées les informations du sol


<center> 

<div><a class="button" href="contact.html">Collaborons !</a></div> 

</center>
</section>
<section>
  <h2>Témoignages</h2>
<%- include('../fragments/temoignages.ejs',{filter:"individuel"}); %>
</section>
<section>



## Comment je travaille?
1. Contact téléphonique : Vous m'expliquez votre projet dans les grandes lignes, je vérifie que je me sens en mesure de vous accompagner sur votre projet
2. Visite : Nous mettons au clair vos besoins, ce à quoi l'accompagnement va aboutir, les moyens de paiement (€, G1, troc...) et nous vérifions que nous souhaitons travailler ensemble
3. Accompagnement pas à pas : Nous programmons des sessions de travail ensemble, de préférence d'une traite.
Nous pouvons aussi convenir d'un accompagnement sur la durée où vous payez à chaque session ou par forfait.

