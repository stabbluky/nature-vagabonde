title: Cours de Conception en Permaculture (CCP)
<<: !!inc/file ../dates.yml
eventFilterSlug: cours-design-permaculture
-------------------
# Cours de Conception en Permaculture (CCP)

<iframe class="portrait" width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.aquilenet.fr/videos/embed/3c74e0af-7d95-4725-aeb8-5560faf2a817" frameborder="0" allowfullscreen></iframe>

## CCP, qu'est-ce que c'est?

Un CCP ou Cours de Conception en Permaculture (PDC en anglais) est un curriculum standardisé de 72h minimum développé dans les années 80 par Bill Mollison,
le co-fondateur de la permaculture, dans le but de faciliter la diffusion de la permaculture par un format standardisé réplicable. Il est reconnu entre pairs au
niveau international. Le curriculum reprend les grandes lignes du livre "Permaculture, a designer's manual" de Bill Mollison.

## Quelle est la particularité de ce CCP par rapport aux autres?

Mon parcours de vie m'a amené à découvrir des innovations, des applications concrètes développés en entreprises,
dans les milieux associatifs ou les collectifs citoyens qui sont en parfait alignement avec l'éthique de la permaculture.
En plus de la base du curriculum, je l'ai **actualisé en y ajoutant ces nouveaux concepts** qui me semblent fondamentaux
pour la construction d'un monde plus juste et en harmonie avec le vivant.

Il comprend **108h de formation** et j'y développe en particulier les sujets de **gouvernance (entreprises, institutions), de monnaie libre et de l'informatique!**

Nous apportons lors de ce CCP autant d'importance au contenu que la forme. Ainsi les temps de repas, les soirées, les temps de pause, etc font partie intégrante
de l'aventure que nous souhaitons vous faire vivre. Il est voulu comme **une expérience immersive** dans un univers d'écoute, de respect, de partage,
de collaboration et de reconnexion au vivant, aux autres et à soi.

Aussi, afin de garantir la qualité de la transmission tout en permettant une bonne dynamique de groupe, nos CCP sont **limités à 20 participants**.

Des **visites** sont prévues pour voir d'autres façons concrètes d'appliquer la permaculture, en particulier :
- à l'[écolieu Jeanot](https://jeanot.fr)
- à la [ferme du Grioué](https://www.facebook.com/lafermedugrioue/), ferme en maraîchage sur sol vivant


[Découvrez le stage en vidéo](https://tube.aquilenet.fr/video-channels/miss_permaculture/videos)


## Pour qui?

Aucun prérequis n'est nécessaire pour suivre un CCP qui sont accessibles à tous publics de 15 à 95 ans!
Une seule condition : être sensible à la dégradation de l'environnement et avoir envie de gagner en perspective pour agir concrètement à son échelle !
Les mineurs sont acceptés sous la responsabilité de leur tuteur légal.

## Pour quoi faire un CCP?

Le monde dans lequel on vit perd de son sens?
Vous vous sentez incompris de votre entourage, vous vous sentez seuls dans les questions existentielles que vous vous posez?
Vous avez peur pour l'avenir de l'humanité sur la planète?
Vous souhaitez contribuer à une transition de société mais vous ne savez pas par où commencer?
Vous envisagez une reconversion professionnelle?
Vous souhaitez apprendre des techniques concrètes que vous pourrez appliquer au quotidien?
Vous voulez cultiver votre jardin dans le respect de la biodiversité et du vivant?
Vous souhaiter intégrer la permaculture au-delà du théorique et l’appréhender de façon plus pratique à travers l'expérience?
Dans tous les cas, un CCP vous donnera des clés pour avancer sur ces questionnements.


<center>

[Regardez les prochaines dates ici](content/fr/dates.yml)


</center>

## Programme

<div class="col">

### Contexte, éthique & principes

- Histoire et contexte de la naissance de la permaculture
- Ethiques et principes
- Principes écologiques de la forêt
- Permaculture et collapsologie
- Pensée systémique

### Design

- méthodologies de design (BOLRADIM, WASPA)
- analyse sectorielle
- échelle permanence (Yeomans, Darren Doherty)

### Ecologie

- Observer et identifier son sol
- Recettes de compostage
- Valoriser l'eau dans le paysage (Keyline design)
- Economiser l'eau au quotidien
- Epuration naturelle des eaux
- Fonctionnement physiologique des plantes
- Comprendre l'arbre pour le soigner
- Plantes bio-indicatrices et plantes sauvages comestibles & médicinales
- Principes d'agro-foresterie et agro-écologie
- Maraîchage manuel sur sol vivant
- Gestion d'un système forestier à risque incendie
- Intégration des animaux dans les écosystèmes

### Bâtiment et énergie

- Autonomie énergétique
- Bioclimatisme et éco-construction
- Urbanisme social

### Outils & technologies

- Outils adaptés
- L'informatique au service de l'humain

### Systèmes économiques

- Histoire et rôle de la monnaie
- Alternatives monétaires et monnaie libre

### Permaculture sociale et gouvernance

- Permaculture humaine
- Les bases de la santé
- Les bases de la nutrition naturopathique et cuisine
- Communication non violente (CNV)
- Systèmes de gouvernance en entreprise ou projets collectifs
- Systèmes de gouvernance citoyenne

</div>


<iframe class="portrait" width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.aquilenet.fr/videos/embed/d6b24bb1-7198-4d11-88e1-423e91a2f66f" frameborder="0" allowfullscreen></iframe>


# Dates à venir

<%
const now = (new Date()).getTime();
const futureEvents = events.filter( (e) => e.dateEnd.getTime()>now && e.type.titleSlug === eventFilterSlug );
futureEvents.sort( (a,b) => a.dateStart.getTime() - b.dateStart.getTime() );
futureEvents.forEach(event=>{
_%>
    <%- include('../fragments/event.ejs',{myEvent:event}); %>
<% }); _%>
