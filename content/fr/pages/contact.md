title: Contact

<<: !!inc/file ../partenaires.yml
-------------------

# Nos coordonnées

<img src="img/contact.jpg" class="portrait"/>


naturevagabonde@riseup.net



## Mes partenaires

_"Seule on va vite, ensemble on va loin"_

Ce proverbe ne saurait être plus juste, car je ne serais pas qui je suis si je n'étais pas entourée, soutenue,
aimée, en premier lieu par mes parents et ma famille, puis enrichie, conseillée et soutenue par mes amis aux qualités et compétences multiples.
J'ai une immense gratitude à leur égard.

Voici un panel non exaustif des personnes avec qui je travaille en collaboration :

<hr/>

<%- include('../fragments/partenaires.ejs'); %>
