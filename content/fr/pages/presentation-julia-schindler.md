title: Qui suis-je? Julia Schindler
altLang:
  en: julia-schindler-resume.md
-------------------

# Qui suis-je ? Julia Schindler


## Entrepreneuse à Co-actions

<center>
<img src="img/coactions-bandeau.jpg"/>

Entrepreneuse à Co-actions, une coopérative d'activité et d'emploi.
A ce titre formation prise en charge

</center>

## Mon histoire

<img src="img/portrait-julia-schindler-miss-permaculture.jpg" class="portrait"/>

Ingénieure agronome franco-américaine, j'ai effectué mon premier CCP en 2010 avec Robyn Francis,
formatrice australienne renommée qui a longtemps enseigné auprès de Bill Mollison, co-fondateur de la permaculture.
Depuis, je ne cesse d'explorer le monde (Europe, Cuba, Inde, Australie...)
à la recherche d'alternatives concrètes à notre société de consommation
que ce soit dans le domaine agricole, le bâtiment,
la construction de projets collectifs ou par la façon de vivre son quotidien.
J'ai même eu l'immense honneur de vivre pendant un mois chez David Holmgren, co-fondateur de la permaculture.

C'est en alternant wwoofing, convergences internationales de permaculture,
congrès scientifiques sur l'agro-foresterie, travail en lycée agricole,
implication dans des associations et divers réseaux,
que je cherche à me tenir à la croisée des savoirs théoriques et pratiques.

Aujourd'hui, en parallèle de mon implication de longue date dans les réseaux alternatifs et de transition,
je développe mon activité de formatrice et accompagnatrice indépendante.
J'ai à cœur de transmettre la philosophie de la permaculture et les clés de l'autonomie
pour encourager chacun à se mettre sur le chemin de contribuer à sa manière
à un monde de respect de soi, des autres et de la Nature.

Je me co-construit avec [mes fréquentations et collaborateurices](contact.html#mes-partenaires).


Merci à Aurélie Monchany pour sa petite interview :

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.aquilenet.fr/videos/embed/d2519852-a156-4491-8347-d376e04899df" frameborder="0" allowfullscreen></iframe>

## Pourquoi Miss Permaculture?

En sortant de mon premier CCP, j'ai vite été vue comme la "Permaculture girl" de mon école d'agro.
Par la suite j'ai souvent eu cette étiquette, parfois vue comme un peu utopique-idéaliste, partout où j'allais.
Au lieu de m'en lamenter, j'ai choisi d'en faire une force, de me costumer et d'en jouer.
Un autre personnage d'où je tire mon inspiration est "Mrs Frizzle",
la maîtresse du Bus magique que je trouve fabuleusement drôle.

C'est en alliant ces contextes que j'ai choisi d'incarner Miss Permaculture
afin de mettre du fun dans mes formations! :)


## Mon parcours permaculturel

- 2019 août - **L'art de vivre ensemble, stage de CNV** (Communication Non Violente) avec [Yoram Mosenzon](http://www.connecting2life.net/en/) et [Oriane Boyer](https://orianeboyer.com) à l'écovillage Ste Cameille, France
- 2019 mai - Congrès mondial d'agroforesterie, Montpellier, France
- 2019 janv - **Systèmes dialogiques, justice et cercles restauratifs**, avec [Dominique Barter](www.cerclesrestauratifs.org/wiki/Dominic_Barter), Bordeaux
- 2018 - **Expérimenter l'entreprise libérée** avec Responsable et Autonome, Bordeaux, France
- 2018 juin - **Formation de formateurs en permaculture** avec Alfred Decker et la guidance de [Rosemarie Morrow](https://www.facebook.com/rosemarymorrowpermacultureaction/) ([Blue Mountains Permaculture](http://www.bluemountainspermacultureinstitute.com.au/about-us/)
- 2018 Intervenante à un **CCP** avec Benjamin Broustey et Aurélie Ruland de [Permaculture Design](http://permaculturedesign.fr/), Dordogne, France
- 2017 nov - Conférencière à la **Convergence Internationale de Permaculture** IPC13 en Inde
- 2017 traductrice au **CCP** de [Starhawk](https://starhawk.org) et Alfred Decker (12P Permaculture) à Permaterra, France
- 2015 août - **Facilitation créative** avec [Robyn Francis](https://robynfrancis.com.au) à Djanbung Gardens, Australie
- 2015 juil - **Design avancé en permaculture** avec [Robyn Francis](https://robynfrancis.com.au) à Djanbung Gardens, Australie
- 2015 Organic soil conference, Australie
- 2015 Biochar convergence, Australie
- 2014 oct - **Keyline™ Design** avec [Darren Doherty](www.regrarians.org), en France
- 2014 juin - **Certification en Management Holistique** [Holistic Management International](https://holisticmanagement.org), NM, USA
- 2013 - **Ambassadeur du jardinage et du bien-vivre alimentaire** avec [Au Ras du Sol](http://aurasdusol.org/), Vélines (24), France
- 2012 - Assistante à un **CCP** avec Pascal Depienne ([Avenir Permaculture](http://avenirpermaculture.fr))
- 2012 - **Convergence Européenne de Permaculture** à Kassel, Allemagne
- 2012 - Assistante à un **CCP** avec Benjamin Broustey et Aurélie Ruland de [Permaculture Design](http://permaculturedesign.fr/), Dordogne, France
- 2012 - **Stage de survie** avec Bernard Bertrand et [Kim Prasche](www.gens-des-bois.org), [Terran](https://www.terran.fr/), France
- 2011 - Diplôme d'ingéniur agronome, spécialité environnement, [Agrocampus Ouest Rennes](https://www.agrocampus-ouest.fr)
- 2011 - employée comme jardinière-paysagiste à [San Isidro Permaculture](https://www.sipermaculture.com), Santa Fe, USA
- 2010 - **Cours de Conception en Permaculture (CCP)** avec [Robyn Francis](https://robynfrancis.com.au) et Andy Darlington



