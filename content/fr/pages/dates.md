title: Dates
#https://www.tablesgenerator.com/markdown_tables#

<<: !!inc/file ../dates.yml

-------------------

Toutes les formations peuvent être financées à titre professionnel.

# Dates à venir

<%
const now = (new Date()).getTime();
const futureEvents = events.filter( (e) => e.dateEnd.getTime()>now );
const pastEvents = events.filter( (e) => e.dateEnd.getTime()<now );
futureEvents.sort( (a,b) => a.dateStart.getTime() - b.dateStart.getTime() );
pastEvents.sort( (a,b) => b.dateStart.getTime() - a.dateStart.getTime() );
futureEvents.forEach(event=>{
_%>
    <%- include('../fragments/event.ejs',{myEvent:event}); %>
<% }); _%>

# Dates passées

<% pastEvents.forEach(event=>{ _%>
    <%- include('../fragments/pastEvent.ejs',{myEvent:event}); %>
<% }); _%>

- *CCP* du 10 au 19 juillet 2020 avec Karine Mery et Julia Schindler à l'école Montessori Les Loupiots à Lougratte (Lot-et-Garonne - 47) : [Infos ici](https://www.facebook.com/asso.kokua/posts/4697257330300568)
- Initiation 27-28 juin 2020 à l'[écolieu Cablanc](https://www.cablanc.com/accueil/cablanc-lecotourisme-au-coeur-des-vignobles-ecotourisme-cablanc-dordogne/), Saussignac (Dordogne - 24)
- (annulé Covid) Initiation 23-24 mai 2020 au [Petit Pontbiel](https://www.petitpontbiel.com/) Pontonx-sur-Adour (Landes - 40)

## Stages divers (initiation à la permaculture, la permaculture au jardin, et autres joyeusetés)


*[En savoir plus sur les stages d'initiation](href="formation.md")*

- du 7 au 11 novembre 2020, *Design Pratique en Permaculture* au [Petit Pontbiel](https://www.petitpontbiel.com/) à Pontonx-sur-l'Adour (40 - Landes)  [Télécharger la brochure (pdf)](img/Stage_de_pratique_de_design.pdf) ;  [Je m''inscris !](https://framaforms.org/inscription-au-stage-de-design-en-permaculture-novembre-2020-petit-pontbiel-landes-1601040295)
- du 11 au 13 décembre, *La permaculture au jardin, principes généraux et travaux d'hiver* au [Petit Pontbiel](https://www.petitpontbiel.com/) à Pontonx-sur-l'Adour (40 - Landes) [Télécharger la brochure(pdf)](content/fr/img/Permaculture_au_jardin.pdf) ; [Je m''inscris !](https://framaforms.org/inscription-stage-la-permaculture-au-jardin-principes-et-travaux-dhiver-decembre-2020-petit-pontbiel)

