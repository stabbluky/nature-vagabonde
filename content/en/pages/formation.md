title: Stages
-------------------
# Stages

<img src="img/formation.jpg" class="portrait"/>

Pourquoi faire un stage?

La permaculture ne pouvant pas se résumer à des techniques,
je considère que c'est surtout par la pratique, l'expérience,
le vécu qu'elle peut s'intégrer réellement.
Pour ma part en tout cas, c'est à l'issue d'un CCP que j'ai pris conscience que
malgré toutes mes lectures sur le sujet auparavant,
c'est vraiment en vivant le CCP que j'ai pu réellement intégrer l'intégralité du concept.
Cette intégration a été réellement transmutatrice pour moi :
tout d'un coup, je pouvais regarder le monde autour de moi
avec les clés fondamentales pour faire partie d'une mouvance d'un monde meilleur
et ainsi réellement incarner la célèbre citation de Gandhi :
"Sois le changement que tu souhaites voir dans le monde".

C'est l'expérience du meilleur de mon vécu à travers les différents stages auxquels j'ai pu assister
qui me tient à cœur de transmettre à travers les stages que j'organise.

Les types de stage que je propose :

## Stages d'initiation à la permaculture

Pour ceux qui ne sont pas prêts à mettre les ressources nécessaires pour entrer pleinement dans un CCP,
c'est un bon moyen de passer de la théorie à l'expérience pratique et avoir un bon aperçu du concept.

<!--
je trouve la tournure culpabilisante/crispante,
ça ne me donne pas envie de faire un CCP au rabais.
Je veux découvrir, ok je suis pas prêt à mettre beaucoup de ressource mais laisse moi découvrir, on verra après !

Contre proposition :
Venez découvrir par la pratique (accompagné de théorie pour la compréhension) la permaculture en un week-end.

Pour approfondir et vous approprier les concepts de la permaculture dans toute leur étendue,
je vous conseille vivement d'aller plus loin en venant participer à un CCP sur 14jours.

-->

[Les prochaines dates sont consultable !](agenda.html)

## Cours de Conception en Permaculture (CCP)

Un CCP ou Cours de Conception en Permaculture est un stage de 72h minimum développé par Bill Mollison,
co-fondateur de la permaculture,
dans le but de diffuser la permaculture et d'en assurer un format standardisé réplicable.

Les CCP que je propose sont des stages un peu plus complet de 91h sur 14 jours.
En plus de la base du curriculum je l'ai actualisé en y ajoutant des nouveaux concepts
s’intégrant parfaitement dans l'esprit de la permaculture
et qui me semble fondamental pour la construction d'un monde plus juste et en harmonie avec le vivant.

[Les inscripition sont ouvertes dès maintenant sur la page des dates.](agenda.html)


## A la demande
Je peux aussi proposer un stage à la demande selon vos intérêt et à domicile dans votre structure. Si je considère que je n'ai pas les compétences adéquates pour répondre à vos attentes, j'ai certainement des contacts dans mon réseau avec lesquels je pourrai vous mettre en relation.
