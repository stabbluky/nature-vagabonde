title: Conferences
-------------------
# Conferences

<img src="img/conference.jpg" class="portrait"/>

_Conférences disponibles, format 1h30 + 30 min de questions réponses ou adaptables à la demande._

- "**Introduction to permaculture**" : conference that sums up the history, concepts and philosophy behind permaculture.
- "**Exploration on the paths of permaculture**" : my journey of discovery of permaculture across the world (France, UK, Germany, Australia, India, Cuba by the discovery of farms, community projects, eco-villges, urban agriculture...)
- "**My life experience at Melliadora at David Holmgren's, co-founder of permaculture, and his partner Sue Denett, my permaculture super hero!**" : An immersion in another way of living, a philosophy integrated in all aspects of everyday life. An illustration of what could be a positive future on a household scale.
- "**Women's permaculture or how to cultivate your secret garden**" : My own experience of how with observation methods of my cycle, I found much more than a natural and reliable contraception method, but most of all a real liberation to start to uncover the mysteries of my own body and to celebrate being a woman.

All publics welcome : women, men, trans-gender, etc.
