title: Who am I ? Julia Schindler
altLang:
  fr: presentation-julia-schindler.md
-------------------

# Who am I ? Julia Schindler

<img src="img/portrait-julia-schindler-miss-permaculture.jpg" class="portrait"/>

French-American I graduated in France from an agronomy school with a master's degree in “Sustainable agriculture and territorial development”. I did my first PDC in 2010 with Robyn Francis, renown Australian permaculture teacher who has taught along with Bill Mollison, co-founder of permaculture. Since then, I keep exploring the world (Europe, Cuba, India, Australia, Togo...) seeking alternatives to our consumerism society, weather it's in the agricultural field, construction, building community or simply by every day habits and way of life. I even had the great privilege to live for a month at Melliadora, Sue Dennett and Davind Holmgren's house (co-founder of permaculture).

It's by alternating between wwoofing, attending to international permaculture convergences or scientific congresses on agroforestry and organic farming, managing a horticultural farm in a French agricultural high-school, getting involved in several non profits and various networks that I seek to integrate theoretical and practical knowledge.

I have been assistant in PDCs with Benjamin Broustey (Permaculture Design, France), Pascal Depienne (Avenir Permaculture, UPP, France), as well as Starhawk (USA) and Alfred Decker (12P Permaculture, Spain) before following a Permaculture Teacher Training class under the supervision of Rosemary Morrow (Blue Mountain, Australia).

Today alongside my previous implications in non-profits and transition networks, I am developing my teacher and consulting business. My goal at heart is to convey the permaculture philosophy through my teachings as well as my way of life, and to share the keys I've found that allow each and every one of us to walk the path of a world with more respect for ourselves, respect of others and respect of Nature.


## Why Miss Permaculture?

After my first PDC, I was known as the school's "Permaculture girl", sometimes in a positive way, other times as a idealistic utopian, sometimes as a way to mock me. I sort of kept this label wherever I went. Instead of seeing it as a problem, I chose to see it as a strength : to wear this title with pride and play. Another place where I take this inspiration is from one of my favorite cartoon characters “Mrs Frizzle” in the Magic School Bus that I used to watch as a kid. The title “Mrs” is not well known in non English speaking countries, that is why I use “Miss”.

It's by integrating these contexts that I choose to embody “Miss Permaculture” (and others!) in order to put some fun in my workshops! :)
