title: Contact
-------------------

<img src="img/portrait-julia-schindler-miss-permaculture.jpg" class="portrait"/>


Julia Schindler

Joyful gleaner and sower of permaculture futures

+33 (0)7 68 77 31 84

contact AT miss-permaculture.com

