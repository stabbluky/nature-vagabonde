title: Accompagnements
-------------------
# Accompagnements

<img src="img/formation.jpg" class="portrait"/>


## Vous souhaitez faire de votre terrain un petit coin de Paradis ?

D'un balcon à une ferme en passant par des terrains collectifs, je vous accompagne pas à pas pour vous aider à vous poser les bonnes questions pour que votre Paradis :

- réponde à vos besoins (autonomie, production alimentaire, loisirs, animaux...)
- valorise les ressources locales
- contribue à plus de biodiversité
- stocke du carbone
- rétablisse les cycles de l'eau et des nutriments

**Attention :** Si vous souhaitez simplement installer un potager et rien changer à vos habitudes, faites plutôt appel à un jardinier-paysagiste. 
Dire que vous souhaitez installer un potager pour "faire de la permaculture" est comme dire que vous devenez bouddhiste simplement parce que vous méditez!

Pour moi la permaculture est toute une démarche de pensée sur laquelle je vous accompagne pas à pas AVEC VOUS sur VOTRE projet. Je ne vous délivre pas un plan clé en main.
Suivre un accompagnement avec moi, c'est comme une formation particulière pour qu'à terme vous soyez autonome dans la logique de conception de votre terrain. J'introduis les concepts à connaître au fur et à mesure.

<center> 

<div><a class="button" href="contact.html">Collaborons !</a></div> 

</center>

## Comment je travaille?
1. Contact téléphonique : Vous m'expliquez votre projet dans les grandes lignes, je vérifie que je me sens en mesure de vous accompagner sur votre projet
2. Visite : Nous mettons au clair vos besoins, les moyens de paiement (€, G1, troc...) et nous vérifions que nous souhaitons travailler ensemble
3. Accompagnement pas à pas : Vous me rémunérez à l'heure de travail et je m'engage à vous fournir le résultat de mes avancées de facon régulière. 
Vous êtes libres de cesser l'engagement à tout moment. Ce système permet une grande flexibilité dans le contrat tout en garantissant l'avancement du projet,
et en évitant qu'il y ait des perdant dans des promesses non tenues.
