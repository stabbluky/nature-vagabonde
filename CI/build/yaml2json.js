const fs = require("fs");
const yaml = require('js-yaml');

function yLoad(fileName){
  return yamlParseInclusions(fs.readFileSync(fileName, 'utf8'), fileName);
}

function yamlParseInclusions(content, fileName){
  return yaml.load(yamlRecursivelyInclude(content, getPath(fileName)));
}
function yamlRecursivelyInclude(content, path){
  content = content.replace(/( *)<<: !!inc\/file (.+)/g, (match, preIndent,fileToInclude) =>{
    const includePath = getPath(path + fileToInclude);
    const rawIncludeContent = fs.readFileSync(path + fileToInclude, "utf8");
    const indentedIncludeContent = rawIncludeContent.split('\n').join(`\n${preIndent}`);
    return yamlRecursivelyInclude(indentedIncludeContent, includePath);
  });
  return content;
}

function getPath(fileName) {
  return fileName.substr(0, fileName.lastIndexOf('/') + 1);
}

const commonConfig = yLoad('content/common/config.yml');
commonConfig.now = (new Date).toISOString();
fs.writeFileSync('generated/config.common.json', JSON.stringify(commonConfig), 'utf8');
const langs = Object.keys(commonConfig.availableLang);
for(let lang of langs){
  const lConfig = yLoad(`content/${lang}/config.yml`);
  fs.writeFileSync(`generated/config.${lang}.json`, JSON.stringify(lConfig), 'utf8');
}
