const fs = require('fs-extra');

async function init(path){
  const langs = (await fs.readdir('content')).filter(str=>str!=='common');
  await fs.ensureDir(path);
  for(let lang of langs){
    await fs.ensureDir(`${path}/${lang}`);
  }
}
init('generated/toPublish');
