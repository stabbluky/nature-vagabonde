const fs = require("fs");
const yaml = require('js-yaml');
const md = require('marked');
md.setOptions({headerIds: true, xhtml: true});
const ejs = require('ejs');

function yamlParseInclusions(content, fileName){
  return yaml.load(yamlRecursivelyInclude(content, getPath(fileName)));
}
function yamlRecursivelyInclude(content, path){
  content = content.replace(/( *)<<: !!inc\/file (.+)/g, (match, preIndent,fileToInclude) =>{
    const includePath = getPath(path + fileToInclude);
    const rawIncludeContent = fs.readFileSync(path + fileToInclude, "utf8");
    const indentedIncludeContent = rawIncludeContent.split('\n').join(`\n${preIndent}`);
    return yamlRecursivelyInclude(indentedIncludeContent, includePath);
  });
  return content;
}

function getPath(fileName) {
  return fileName.substr(0, fileName.lastIndexOf('/') + 1);
}

const commonConfig = JSON.parse(fs.readFileSync('generated/config.common.json', 'utf8'));
commonConfig.now = (new Date).toISOString();
//commonConfig.importYamlFile = importYamlFile;
const langs = Object.keys(commonConfig.availableLang);
const langConfigs = {};
for(let lang of langs) langConfigs[lang] = merge(commonConfig, JSON.parse(fs.readFileSync(`generated/config.${lang}.json`)));

for(let lang of langs) fs.readdirSync(`content/${lang}/pages/`).forEach((f)=>buildPage(`content/${lang}/pages/${f}`,lang));
// build sitemap
const rawSitemap = fs.readFileSync(`content/common/sitemap.xml.ejs`, 'utf8');
for(let lang of langs) {
  langConfigs[lang].canonical = commonConfig.canonical;
  const sitemapContent = ejs.render(rawSitemap,langConfigs[lang],{filename:`content/${lang}/sitemap.xml`});
  fs.writeFileSync(`generated/toPublish/${lang}/sitemap.xml`,sitemapContent,"utf8");
}

function buildPage(filePath,lang) {
  const rawFile = fs.readFileSync(filePath, 'utf8');
  const splitFile = rawFile.replace(/(\r\n|\r|\n)\s*---+\s*(\r\n|\r|\n)/,'---***---').split('---***---');
  let specificOpt = {};
  if(splitFile.length===2) specificOpt = yamlParseInclusions(splitFile.shift(),filePath);
  let fileContent = splitFile.shift();
  const thisLangConf = clone(langConfigs[lang]);
  const otherLangs = langs.filter(str=>str!==lang);
  otherLangs.forEach(l=>{
    const orgLPath = filePath.replace(`/${lang}/`,`/${l}/`);
    if(fs.existsSync(orgLPath)) thisLangConf.altLang[l]= filePath.split('/').pop();
  });
  const opt = merge(thisLangConf,specificOpt);
  opt.fileExt = filePath.split('.').pop();
  opt.fileName = filePath.split('/').pop().split('.').shift();
  opt.activeLang = lang;

  fileContent = ejs.render(fileContent,opt,{filename:filePath});
  if(opt.fileExt==='md') fileContent = `<section>${md(fileContent,opt)}</section>`;

  const templatePath = `content/common/${opt.template}.ejs`;
  const template = fs.readFileSync(templatePath, 'utf8');
  fileContent = template.split('{{{mainContent}}}').join(fileContent);
  fileContent = fileContent.replace(/{{([^}]+)}}/g,(match,keyword)=>opt[keyword]);

  fileContent = ejs.render(fileContent,opt,{filename:templatePath});

  let outputPath = filePath.split(`/`).pop();
  outputPath = `generated/toPublish/${lang}/${outputPath}`;
  outputPath = fileNameExt2Html(outputPath,opt.fileExt);
  fileContent = fileNameExt2Html(fileContent,opt.fileExt);

  fs.writeFileSync(outputPath,fileContent,"utf8");

  // prepare for sitemap
  langConfigs[lang].activeLang = lang;
  if(!langConfigs[lang].pages) langConfigs[lang].pages = [];
  opt.altLang[lang] = outputPath.split(`/`).pop();
  langConfigs[lang].pages.push({
    fileName:outputPath.split(`/`).pop(),
    hidden:!!opt.hidden,
    altLang: JSON.parse(fileNameExt2Html(JSON.stringify(opt.altLang),opt.fileExt))
  });
}

function fileNameExt2Html(str,orignalFileExt){
  return str.replace(new RegExp(`\\.${orignalFileExt}`,'g'),'.html');
}

function clone(json) {
  return JSON.parse(JSON.stringify(json))
}

function merge(baseJson, overwritingJson) {
  if(typeof baseJson !== "object") return clone(overwritingJson);
  let res = clone(baseJson);
  for (let key in overwritingJson) {
    if (typeof overwritingJson[key] === "object" && typeof res[key] !== 'undefined') {
      res[key] = merge(res[key], overwritingJson[key]);
    }
    else res[key] = overwritingJson[key];
  }
  return res;
}
