const fs = require('fs-extra');

async function init(path){
  const langs = (await fs.readdir('content')).filter(str=>str!=='common');
  await fs.ensureDir(path);
  await fs.copy(`content/common/img`,`${path}/img`,{dereference:true});
  for(let lang of langs){
    await fs.ensureDir(`${path}/${lang}`);
    await fs.copy(`${path}/img`,`${path}/${lang}/img`,{dereference:true});
    await fs.copy(`content/${lang}/img`,`${path}/${lang}/img`,{dereference:true});
  }
}
init('generated/toPublish');
